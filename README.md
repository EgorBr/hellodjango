```
#!bash

$ cd ~/env_mysite

$ . bin/activate				#activate virtual env

$ cd ~/env_mysite/mysite
$ python manage.py runserver		#start dev server

$ cd ~/env_mysite/mysite/mysite		#user data folder
├── __init__.py
├── ....
├── settings.py
├── urls.py
├── views.py
└── wsgi.py

# It's convenient to open ~/env_mysite/mysite/mysite folder in PyCharm.
# The two most important files: urls.py, views.py
```