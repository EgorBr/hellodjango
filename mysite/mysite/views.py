from django.http import HttpResponse
from django.template import Context, loader
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
import datetime

#TODO: use render everywhere, instead of HttpResponse!
#This is python 2.7.12.

import sys
print(sys.version)
print('---GO-GO-GO---')
#User.objects.create_user(username='egor', password='1234') #unique

#Django does not store passwords, only hash.
#Authentification - using django, other stuff - using sqlite.


def index(request):
    template = loader.get_template('index.html')
    my_list = [2, 4, 64, 1024]
    context = Context()
    context['new_var'] = 42
    context['name'] = 'egor'
    context['my_list'] = my_list
    return  HttpResponse(template.render(context))

def log_in(request):
    return render(request, 'login.html', {'foo' : 'bar'})

def login_check(request):
    username = request.POST['username']
    password = request.POST['password']

    user = authenticate(username=username, password=password)
    if user is not None:
        print('AUTH: OK')
        login(request, user)
    else:
        print('AUTH: FAIL')

    return HttpResponse('Your username: {0}. Your password: {1}'.format(username, password))
    #return render(request, "index.html", {'foo':'bar'})

@login_required
def secret_page(request):
    return HttpResponse('This is the secret page! Your username: {0}'.format(request.user.username))

def hello(request):
    if request.method == 'GET':
            print('first parameter:'),
            print(request.GET['param1'])
    return HttpResponse('<b>Hello world</b>')

def current_datetime(request):
    now = datetime.datetime.now()
    html = 'It is %s.' % now
    return HttpResponse(html)
